icu4j-4.4 (4.4.2.2-4) unstable; urgency=medium

  * Team upload.
  * Fixed the build failure with OpenJDK 17 (Closes: #981953)
  * No longer install the change report
  * Standards-Version updated to 4.5.1
  * Switch to debhelper level 13
  * Use salsa.debian.org Vcs-* URLs
  * Updated the watch file and removed debian/orig-tar.sh

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 08 Feb 2021 13:15:10 +0100

icu4j-4.4 (4.4.2.2-3) unstable; urgency=medium

  * Team upload.
  * Fixed the build failure with Java 9 (Closes: #875408)
  * Removed Niels Thykier from the uploaders (Closes: #770577)
  * Standards-Version updated to 4.1.3
  * Switch to debhelper level 11
  * Use secure Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 08 Mar 2018 16:49:08 +0100

icu4j-4.4 (4.4.2.2-2) unstable; urgency=medium

  * Team upload.
  * Fixed build.xml to recognize Java 8
  * Do not compile the ICUTaglet class which relies on internal JDK classes
    no longer available in Java 8.
  * debian/control:
    - Standards-Version updated to 3.9.5 (no changes)
    - Fixed a typo in the package description (Closes: #711392)
  * Switch to debhelper level 9

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 14 Apr 2014 12:16:38 +0200

icu4j-4.4 (4.4.2.2-1) unstable; urgency=low

  * New upstream release
  * Add Jakub Adam to Uploaders
  * Bumped Standards-Versions to 3.9.3 - no changes required.
  * Updated Vcs-Browser field.

 -- Jakub Adam <jakub.adam@ktknet.cz>  Fri, 09 Mar 2012 17:34:49 +0100

icu4j-4.4 (4.4.2-1) unstable; urgency=low

  * New upstream release
  * Added a README.source because finding the source has become
    less intuative.
  * Bumped Standards-Versions to 3.9.2 - no changes required.
  * Updated the Vcs-* fields.

 -- Niels Thykier <niels@thykier.net>  Tue, 02 Aug 2011 15:50:33 +0200

icu4j-4.2 (4.2.1.1-1) unstable; urgency=low

  [ Niels Thykier ]
  * New upstream release. (Closes: #583808)
  * Renamed package since eclipse 3.5.2 depends on 4.0.1.1.
  * Removed dependencies on JREs - per the Java Policy this is no
    longer required.
  * Fixed a typo in the package synopsis.
  * Rewrote d/rules to use DH7 with javahelper.
  * Removed the patch; not required anymore.
  * Bumped Standards-Version to 3.8.4 - no changes required.
  * Removed NEWS and README.Debian; they were no longer relevant.

  [ Torsten Werner ]
  * Implement a get-orig-source target in debian/rules.

 -- Torsten Werner <twerner@debian.org>  Wed, 23 Jun 2010 21:32:29 +0200

icu4j (4.0.1.1-1) unstable; urgency=low

  [ Andreas Tille ]
  * Watch file now detects all versions even for new major
    version change

  [ Niels Thykier ]
  * New upstream release.
    - Added patch to handle hardcoded versions.
  * Bumped debhelper compat to 7.
  * Bumped Standards-Version to 3.8.3.
    - Changed section to java.
  * Added myself to Uploaders.
  * Removed eclipse from Build-Depends to avoid circular
    Build-Dependency (eclipse 3.5 depends on icu4j).
  * Converted source to 3.0 (quilt).
  * Bumped version in NEWS to match first affected version
    released in Debian.
  * Added a missing ${misc:Depends}.

 -- Niels Thykier <niels@thykier.net>  Thu, 21 Jan 2010 17:15:20 +0100

icu4j (3.8.1-1) unstable; urgency=low

  * Initial upload to Debian
    Closes: #508777
  * Maintainer: Debian Java maintainers
    <pkg-java-maintainers@lists.alioth.debian.org>
  * Added myself and Steffen Moeller as Uploader
  * Removed Homepage field from long description
  * Added VCS fields
  * Fixed syntax of debian/NEWS file

 -- Andreas Tille <tille@debian.org>  Mon, 15 Dec 2008 09:36:51 +0100

icu4j (3.8.1-0ubuntu1~ppa5) intrepid; urgency=low

  * Update to standards version 3.8.0 (no changes needed.)

 -- Nathan Summers <rock@gimp.org>  Wed, 05 Nov 2008 19:11:27 -0500

icu4j (3.8.1-0ubuntu1~ppa4) hardy; urgency=low

  * Fixed empty jar creation.

 -- Nathan Summers <rock@gimp.org>  Mon, 03 Nov 2008 21:08:44 -0500

icu4j (3.8.1-0ubuntu1~ppa3) hardy; urgency=low

  * Fixed symbolic link.

 -- Nathan Summers <rock@gimp.org>  Mon, 03 Nov 2008 19:31:45 -0500

icu4j (3.8.1-0ubuntu1~ppa2) hardy; urgency=low

  * Fixed install location.
  * "clean" target now undoes the preprocessor mangling.

 -- Nathan Summers <rock@gimp.org>  Mon, 03 Nov 2008 18:32:27 -0500

icu4j (3.8.1-0ubuntu1~ppa1) hardy; urgency=low

  * New upstream version.
  * Builds Eclipse-compatible version of jar.

 -- Nathan Summers <rock@gimp.org>  Thu, 25 Sep 2008 18:52:02 -0400

icu4j (3.4.5-1ubuntu1) edgy; urgency=low

  * New upstream release.
  * Changed rules to use CDBS.
  * Changed build and runtime dependencies.
  * Upgraded standards version to 3.7.2.

 -- Vladimír Lapáček <vladimir.lapacek@gmail.com>  Wed, 27 Sep 2006 17:04:02 +0200

icu4j (3.4.4-1) unstable; urgency=low

  * Initial release.

 -- Steffen Moeller <steffen_moeller@gmx.de>  Fri,  5 May 2006 19:49:26 +0200

