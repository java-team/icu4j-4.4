/*
 *******************************************************************************
 * Copyright (C) 2011, International Business Machines Corporation and         *
 * others. All Rights Reserved.                                                *
 *******************************************************************************
 */
package com.ibm.icu.util;

/*
 * Empty stub
 */
public class CurrencyAmount {
    private CurrencyAmount() {}
}
